package com.example.insomnia.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Settings extends AppCompatActivity {

    protected TextView ip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Button save= (Button)this.findViewById(R.id.Save);
        ip=(TextView) this.findViewById(R.id.ipAddress);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControllerService.get().setIP(ip.getText().toString());
                 new Thread(new Runnable() {
                    @Override
                    public void run()
                    {
                        ControllerService.get().connectSocket();
                    }
                }).start();
                finish();
            }
        });
    }
}
