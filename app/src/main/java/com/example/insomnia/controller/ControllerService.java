package com.example.insomnia.controller;

import android.util.Log;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;


public class ControllerService {

    private int player;
    private String ip;
    private Socket s;

    private static ControllerService service;

    public static ControllerService get()
    {
        if(service==null)
        {
            service=new ControllerService();
        }
        return service;
    }

    public ControllerService()
    {
        player=1;
        ip="";
    }

    public void connectSocket()
    {
        try
        {
            s=new Socket(InetAddress.getByName(ip),4001);

        }
        catch(IOException e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public Socket getSocket()
    {
        return s;
    }

    public void setPlayer(int p)
    {
        player=p;
    }

    public int getPlayer()
    {
        return player;
    }

    public void setIP(String ip)
    {
        this.ip=ip;
    }

    public String getIP()
    {
        return ip;
    }

    public void Up()
    {
                try {
                    PrintWriter out = new PrintWriter(s.getOutputStream(), true);
                    out.println("Wd");
                }
                catch(Exception e)
                {
                    Log.i("Error",e.getMessage());
                }
                    //RestTemplate restTemplate = new RestTemplate();
                    //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
                    //restTemplate.postForEntity("http://" + ip + ":8080/up?player=" + player, null, String.class);

    }

    public void UpS()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Wu");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }


    }

    public void Down()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Sd");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void DownS()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Su");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }


    }

    public void Left()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Ad");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void LeftS()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Au");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void Right()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Dd");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void RightS()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Du");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }

    }

    public void A()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Zd");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void AS()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Zu");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void B()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Xd");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void BS()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Xu");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void Start()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Ed");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void Starts()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Eu");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void Select()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Qd");
        }
        catch(Exception e)
        {
            Log.i("Error",e.getMessage());
        }
    }

    public void SelectS()
    {
        try {
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("Qu");
        }
        catch(Exception e)
        {
            Log.i("Error:",e.getMessage());
        }
    }


}
