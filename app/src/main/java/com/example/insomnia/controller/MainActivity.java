package com.example.insomnia.controller;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.concurrent.atomic.AtomicBoolean;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


        ((ImageButton) this.findViewById(R.id.UpButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                       new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().Up();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_UP:
                        new  Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().UpS();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().UpS();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });

        ((ImageButton) this.findViewById(R.id.DownButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().Down();
                                return;
                            }
                        }).start();

                        return true;
                    case MotionEvent.ACTION_UP:
                        new  Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().DownS();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().DownS();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });

        ((ImageButton) this.findViewById(R.id.LeftButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                       new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().Left();
                                return;
                            }
                        }).start();

                        return true;
                    case MotionEvent.ACTION_UP:
                        new  Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().LeftS();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().LeftS();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });

        ((ImageButton) this.findViewById(R.id.RightButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                            new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().Right();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_UP:
                        new  Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().RightS();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().RightS();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });

        ((Button) this.findViewById(R.id.AButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                       new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().A();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_UP:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().AS();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().AS();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });

        ((Button) this.findViewById(R.id.BButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().B();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_UP:
                        new  Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().BS();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().BS();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });

        ((Button) this.findViewById(R.id.Start)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().Start();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_UP:
                        new  Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().Starts();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().Starts();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });

        ((Button) this.findViewById(R.id.Select)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                      new Thread(new Runnable() {
                            @Override
                            public void run() {

                                ControllerService.get().Select();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_UP:
                        new  Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().SelectS();
                                return;
                            }
                        }).start();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        return true;
                    case MotionEvent.ACTION_OUTSIDE:
                        return true;
                    default:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ControllerService.get().SelectS();
                                return;
                            }
                        }).start();
                        return true;
                }
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent intent=new Intent(this,Settings.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
